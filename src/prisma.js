import {Prisma} from 'prisma-binding'
import { json } from 'body-parser';

const prisma = new Prisma({
    typeDefs:'src/generated/prisma.graphql',
    endpoint:'http://localhost:4466'
})

export { prisma as default}
//
//prisma.query.tasks(null,'{title}').then((data)=>{
 //console.log(JSON.stringify(data,undefined,2))})
